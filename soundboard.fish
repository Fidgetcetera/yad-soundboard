#!/usr/bin/env fish
# Requires Yad, Fish, and Sox (for the play command, but you COULD replace that) To use, simply make a 'sounds' folder in the same directory as the original script.

export PULSE_SINK="soundboard_null_sink" #Set this to the "sink" (pulseaudio or pipewire device you want to output sound to.)
set sox_volume 1 # Volume slider not implemented.
set sounds_path (dirname (realpath (status -f)))"/sounds/" #By default, gets the actual directory this script is in, to get the list of all sound files. Uses realpath, so don't worry about symlink messiness.
if not set -q $argv[1]  #If you specify a path as an argument, it'll make that where the sounds are instead.
    set sounds_path $argv[1]
end
set list_of_dirs (find $sounds_path -type d) #Getting a list of directories, including the sounds directory.
set number_of_dirs (count $list_of_dirs)
set fkey (random)
set inc 1
set tab_array ""


#assembling a new, more hellish yad command
for dir in $list_of_dirs
    set dir_button_array "--form"
    set --append tab_array '--tab='(basename $dir) #Makes a label for each subdirectory.    
    for sound in (find $dir -maxdepth 1 -type f | sort) #Gets all files in "sounds". Must be a playable file according to sox, so MP3s, flacs, most audio files should be fine. Symlinks are also fine.
        	set sound_name (string split --right --max 1 --field 1 . (basename $sound)) #Gets the name of the sound, based on the filename.
        	set play_command "play --volume $sox_volume "(string escape -n $sound) # Tells it to play.
        	set button --field=$sound_name:fbtn $play_command # Assembles the initial form field entry...
        	set --append dir_button_array $button #And adds the whole thing to the array.
    end
    yad --plug "$fkey" --tabnum=$inc $dir_button_array --columns=2 &
    set inc (math $inc+1)
end
yad --notebook --key="$fkey" --button="Stop all sound:pkill play" --title="YAD Soundboard" $tab_array
