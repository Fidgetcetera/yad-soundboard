#YAD Soundboard

A simple YAD-based soundboard. Requires `fish`, `yad`, and sox's `play` command. 

To use, download `soundboard.fish` in your favorite way. Place it in any old directory, make a folder called sounds (`mkdir sounds`) in the same directory, put audio files in `sounds`, or subdirectories therein, then just mark executable (`chmod +x soundboard.fish`) as needed, and run `soundboard.fish`.

You can now also run `soundboard.fish /path/to/all/my/funny/meme/sounds/` instead of the default directory located near the script.

You're probably on linux, and might be using Pipewire. If both of those are true, we'd recommend making a virtual duplex audio device:
`pactl load-module module-null-sink media.class=Audio/Duplex sink_name="soundboard_null_sink" sink_properties=device.description="soundboard_null_sink"`

Then output it to the same place as your mic. Go a step further, even - Route it to a second duplex that your mic goes to. Make *that* duplex your true microphone. Fuck around. Find out.
